<h1 align='center'> 码上致富 闲置交易,二手交易,二手商城,同城交易,社交商城类APP源码,支持多国语言</h1>


## 介绍

基于uniapp开发的一款支持多国语言的闲置交易,二手交易,二手商城,同城交易,社交商城类APP源码,大致功能如下：

![闲置交易APP功能导图](https://mszfgit.oss-cn-hangzhou.aliyuncs.com/mszh-goshop/20220929140849.png)

内置聊天功能,可用户二手交易,以及社交商城等等，支持多国语言,对接了paypal,google登陆,facebook登陆,可直接上架国外市场运营。

支持定制，有需求的可联系我们定制，价格美丽，包你满意。

## 效果


### 英文
![Image text](https://mszfgit.oss-cn-hangzhou.aliyuncs.com/mszh-goshop/20220929135508.png)
![Image text](https://mszfgit.oss-cn-hangzhou.aliyuncs.com/mszh-goshop/20220929135517.png)

### 中文
![Image text](https://mszfgit.oss-cn-hangzhou.aliyuncs.com/mszh-goshop/20220929135540.png)
![Image text](https://mszfgit.oss-cn-hangzhou.aliyuncs.com/mszh-goshop/20220929135532.png)
![Image text](https://mszfgit.oss-cn-hangzhou.aliyuncs.com/mszh-goshop/20220929135619.png)
![Image text](https://mszfgit.oss-cn-hangzhou.aliyuncs.com/mszh-goshop/20220929135633.png)
![Image text](https://mszfgit.oss-cn-hangzhou.aliyuncs.com/mszh-goshop/20220929135907.png)
![Image text](https://mszfgit.oss-cn-hangzhou.aliyuncs.com/mszh-goshop/20220929135934.png)
![Image text](https://mszfgit.oss-cn-hangzhou.aliyuncs.com/mszh-goshop/20220929140007.png)
![Image text](https://mszfgit.oss-cn-hangzhou.aliyuncs.com/mszh-goshop/20220929145708.png)
![Image text](https://mszfgit.oss-cn-hangzhou.aliyuncs.com/mszh-goshop/20220929135642.png)
![Image text](https://mszfgit.oss-cn-hangzhou.aliyuncs.com/mszh-goshop/20220929135700.png)
![Image text](https://mszfgit.oss-cn-hangzhou.aliyuncs.com/mszh-goshop/20220929135716.png)
![Image text](https://mszfgit.oss-cn-hangzhou.aliyuncs.com/mszh-goshop/20220929135734.png)


公众号：

![Image text](https://mszfgit.oss-cn-hangzhou.aliyuncs.com/connect/qrcode.jpg)

客服微信：

![Image text](https://mszfgit.oss-cn-hangzhou.aliyuncs.com/connect/wx.jpg)