import App from './App'

// 全局mixins，用于实现setData等功能';
import Mixin from '@/common/mixins.js';
import messages from './locale/index'
import utils from '@/utils/utils.js'
//import tim from '@/utils/tim.js'

import {VueJsonp} from 'vue-jsonp'



// #ifndef VUE3
import Vue from 'vue'
import uView from "uview-ui";
import VueI18n from 'vue-i18n'

// 全局存储 vuex 的封装
import store from './store'
import {router,RouterMount} from '@/common/router.js'  //路径换成自己的

// #ifdef MP
// 引入uView对小程序分享的mixin封装
const mpShare = require('@/uni_modules/uview-ui/libs/mixin/mpShare.js')
Vue.mixin(mpShare)
// #endif

Vue.use(uView);
Vue.mixin(Mixin)
Vue.use(VueI18n)
Vue.use(router);
Vue.use(VueJsonp)

Vue.prototype.$store = store
Vue.prototype.$utils = utils;
//Vue.prototype.$tim =  tim;

Vue.prototype.$onLaunched = new Promise(resolve => {
    Vue.prototype.$isResolve = resolve
})

let i18nConfig = {
  locale: uni.getLocale(),
  messages
}
const i18n = new VueI18n(i18nConfig)

Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
	  store,
	  i18n,
    ...App
})

// 引入请求封装，将app参数传递到配置中
require('@/common/http.interceptor.js')(app)

//v1.3.5起 H5端 你应该去除原有的app.$mount();使用路由自带的渲染方式
// #ifdef H5
	RouterMount(app,router,'#app')
// #endif

// #ifndef H5
	app.$mount(); //为了兼容小程序及app端必须这样写才有效果
// #endif
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
import { createI18n } from 'vue-i18n'
const i18n = createI18n(i18nConfig)
export function createApp() {
  const app = createSSRApp(App)
  app.use(i18n)
  return {
    app
  }
}
// #endif