import jwt from '@/utils/jwt.js';
import {cacheKey, commonConstant} from '@/common/constant.js';
import { memberInfo,logout } from '@/common/http.api.js';
const member = {
    state: {
        token: jwt.getAccessToken(),
        isLogin: jwt.getLogin(), // 是否登陆
        memberInfo: jwt.getUserInfo(), // 会员信息
    },
    mutations: {
        IS_LOGIN(state, data) {
            state.isLogin = data;
            uni.setStorageSync(cacheKey.IS_LOGIN, data);
        },
        TOKEN(state, payload) {
            state.token = payload;
            uni.setStorageSync(cacheKey.TOKEN, payload);
        },
        // 用户信息
        MEMBER_INFO(state, data) {
            state.memberInfo = data;
            uni.setStorageSync(cacheKey.MEMBER_INFO, data);
        },
    },
    actions: {
		resetMemberInfo({
                        commit,
                        dispatch,
                        getters,
                        state
                    }, token = ''){
			commit('MEMBER_INFO', {});
		},
        // 获取用户信息
        async getMemberInfo({
                        commit,
                        dispatch,
                        getters,
                        state
                    }, token = '') {
            return new Promise((resolve, reject) => {
                token && commit('TOKEN', token);
                memberInfo().then(res => {
                    if (res.code === 0) {
                        commit('MEMBER_INFO', res.data);
                        commit('IS_LOGIN', true);
                        resolve(res.data)
                    }
                }).then(() => {
                    
                }).catch(e => {
                        reject(e)
                    })
            })
        },
		LogOut({ commit, state }){
		  return new Promise((resolve, reject) => {
			logout().then(res => {
				resolve()
			}).catch(error => {
			  reject(error)
			})
		  })
		}
    },
    getters:{
        token: state => state.token,
        isLogin: state => state.isLogin,
        memberInfo: state => state.memberInfo,
        memberData: state => state.memberData,
    }
}

export default member