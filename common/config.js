/* 定制开发请联系客服微信：csmszf */
const config = {
	// 产品名称
	productName: 'go商城',
	// 名称
	companyName: '码上致富',
	// 应用版本号
	appVersion: '1.0.0',
	// 管理基础路径
	taokePath: '/goshop',
	apiPath: '/api',
	paypal:{
		clientId:'AcmgoUbh2XeJUIxmMtb9kynGduHnyT7IE6fhtd-wMJV1vdpeUrfQfIzHHPrts33MSQvTFuLa06Arq3l2',
		//mode:"sandbox"
		mode:"live"
	}
}

config.tenantId='1';
config.baseUrl = 'http://localhost:8077/app-api';
//config.baseUrl = 'http://fendo.mynatapp.cc/app-api';
//config.baseUrl = 'http://fendo.mynatapp.cc/app-api';
//config.baseUrl = 'http://39.105.97.89:8098/prod-api/app-api';
config.appUrl = 'http://mszfmh.oss-cn-hangzhou.aliyuncs.com/';

export default config;