/**
 * 缓存key
 */
export const cacheKey = {
    //===============缓存键的定义===============
    ACCOUNT: "account",
    PASSWORD: "password",
    IS_LOGIN: "is_login",
    TOKEN: "token",
    SESSION_ID: "session_id",
    MEMBER_INFO: "member_info",
    MEMBER_DATA: "member_data",
    MEMBER_OPEN_ID: "user_open_id",
    ADDRESS_JSON: 'address_json',
}

/**
 * 通用常量
 */
export const commonConstant = {
    //默认头像
    avatar :'http://fendotk.oss-cn-beijing.aliyuncs.com/file/1/2022/04/18/6e5cfb05827c56b95adf9963c94107ab.png',
}

